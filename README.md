# SKA GitLab Runner Management Tool

## Requirements

This repository is intended to be used in [BIFROST](https://gitlab.com/ska-telescope/sdi/bifrost) machine. However, it is possible to use it on your local machine providing:

- Set up a GitLabToken with `api` scope in `conf.ini` file
- Correctly configure `cloud.yaml` to access openstack OR have necessary environment variables available (_Note: This step is not necessary if you are already using this in BIFROST_)

_The system used for development needs to have Python 3, `pip`, and ansible installed._

## Install

**Always** use a virtual environment. [Pipenv](https://pipenv.readthedocs.io/en/latest/) is now Python's officially
recommended method, but we are not using it for installing requirements when building on the CI Pipeline. You are encouraged to use your preferred environment isolation (i.e. `pip`, `conda` or `pipenv` while developing locally.

For working with `Pipenv`, follow these steps at the project root:

First, ensure that `~/.local/bin` is in your `PATH` with:

```bash
> echo $PATH
```

In case `~/.local/bin` is not part of your `PATH` variable, under Linux add it with:

```bash
> export PATH=~/.local/bin:$PATH
```

or the equivalent in your particular OS.

Then proceed to install pipenv and the required environment packages:

```bash
> pip install pipenv # if you don't have pipenv already installed on your system
> pipenv install
> pipenv shell
```

You will now be inside a pipenv shell with your virtual environment ready.

Use `exit` to exit the pipenv environment.

## Using this repository

First, ensure you satisfy the [Requirements](#Requirements) section.

This repository is for managing gitlab runners currently running in Engage Platform.

You can use make targets to achieve what you want to observe/manage.

Basically,

1. It uses ansible to extract gitlab runners from known machines (in `runnerhosts` file)
2. make targets to perform different operations on runners

### Features

#### Listing runners

To list runner you can use `make list_old_runners`, `make list_k8s_runners` or `make list_all_runners` commands.

- `make list_old_runners`: lists runners with the tag `engageska`
- `make list_k8s_runners`: lists runners with the tag `autok8s`
- `make list_all_runners`: lists the above runners

They all output a list similar to below:

```bash
    Name          id      Shared?   Status      Last Contacted at               Tags                 Projects/Groups
========================================================================================================================
gitlab-runner   1068486   False     online   2020-08-18T14:44:57.476   ['engageska', 'ska',      ['sdp-par-model',
                                             Z                         'sdp', 'docker']          'RASCIL', 'sdp-
                                                                                                 prototype', 'telescope-
                                                                                                 model', 'sdp-dal-
                                                                                                 prototype', 'rascil-
                                                                                                 docker']
------------------------------------------------------------------------------------------------------------------------
gitlab-runner   905106    False     online   2020-08-18T14:44:50.662   ['engageska', 'ska',      ['sim-atmosphere',
                                             Z                         'sdp', 'docker']          'RASCIL', 'sdp-
                                                                                                 prototype', 'telescope-
                                                                                                 model', 'sdp-dal-
                                                                                                 prototype', 'rascil-
                                                                                                 docker', 'sdp-par-
                                                                                                 model']
```

### Listing Runner Machines

To list runner host machines run `make list_runner_machines`. It will output an example as below:

```bash
  Runner Machine Name                 id               Flavor     vCPUs   RAM(MB)   Disk (GB)            Image
========================================================================================================================
docker-executor-TMC-2      0db3c190-6bca-4141-bd4c-   m2.small    4       16384     150         Ubuntu 18.04 LTS
                           89dd46f38459                                                         Jan/2020
------------------------------------------------------------------------------------------------------------------------
helm-runner                3c568070-f6b4-40b0-91b8-   m1.small    1       2048      50          Ubuntu-18.04-x86_64
                           a2df37a3c189
------------------------------------------------------------------------------------------------------------------------
skampi-runner-1-GitLabRu   d93e2591-69e0-4c17-a066-   r1.medium   2       4096      None        None
nnerNode-72crkj33gy5i      79a963286e14
------------------------------------------------------------------------------------------------------------------------
generic-runner             810584bb-5e3f-4469-b684-   m2.small    4       16384     100         Ubuntu 18.04 LTS
                           90111df4f2ef                                                         Jan/2020
------------------------------------------------------------------------------------------------------------------------
docker-executor-TMC        8838b716-c236-45ad-b537-   m2.small    4       16384     200         Ubuntu-18.04-x86_64
                           f4a2f8b46dc8
------------------------------------------------------------------------------------------------------------------------
Perentie-Runner            21acf411-0855-4138-adb0-   r1.medium   2       4096      50          Ubuntu 18.04 LTS
                           f562ab9cb450                                                         Jan/2020
------------------------------------------------------------------------------------------------------------------------
tango-runner-1-GitLabRun   fe0d99c7-dc44-4a71-b033-   r1.medium   2       4096      None        None
nerNode-icxoio6lzkhw       2919be036337
------------------------------------------------------------------------------------------------------------------------
tmc-runner-1-GitLabRunne   f3dedec5-df60-4775-896c-   r1.medium   2       4096      None        None
rNode-msmhbqxbx3dh         5cb4834a153e
------------------------------------------------------------------------------------------------------------------------
generic-runner-3           ad11ec3c-3e93-452b-89ca-   m2.small    4       16384     100         Ubuntu 18.04 LTS
                           8bdea10083e3                                                         Jan/2020
```

### Checking Runners

To check gitlab runners with Engage Platform and list runners **not hosted in Engage Platform**, run `make check_runners`.

### Updating Runners

To update a runner configuration:

1. make sure `RUNNER_ID` is set
2. `runnerconf.ini` file will be populated with the current configuration of the runner
3. change the configuration values in `runnerconf.ini` file.
4. then run `make update_runner`.

To delete a runner, run `make delete_runner`.
