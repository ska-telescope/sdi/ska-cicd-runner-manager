# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

GITLAB_TOKEN ?=
LOGGING_LEVEL ?= ERROR
RUNNER_ID ?= ## Runner id used in delete_runner
RUNNER_NAME ?= ## Runner machine name used in get_machine_info

# define overides for above variables in here
-include PrivateRules.mak

.PHONY: all test lint help
.DEFAULT_GOAL := help

clean:  ## Clean build
	rm -rf build

requirements:  ## install the requirements for project
	pip3 install pipenv
	pipenv --python 3.7
	pipenv install

build: clean  ## Build runner
	 pipenv run python setup.py build

all: test lint

# The following steps copy across useful output to this volume which can
# then be extracted to form the CI summary for the test procedure.
test:

	 python setup.py test | tee ./build/setup_py_test.stdout; \
	 mv coverage.xml ./build/reports/code-coverage.xml;

# The following steps copy across useful output to this volume which can
# then be extracted to form the CI summary for the test procedure.
lint: requirements

	# FIXME pylint needs to run twice since there is no way go gather the text and junit xml output at the same time
	pipenv run pip3 install pylint2junit; \
	pipenv run pylint --output-format=parseable src | tee ./build/code_analysis.stdout; \
	pipenv run pylint --output-format=pylint2junit.JunitReporter src > ./build/reports/linting.xml;


.PHONY: all test lint help clean requirements build

list_runner_machines: ## List GitLab Runner Server Informations for Engage Platform (not k8s runners)
	GITLAB_TOKEN="$(GITLAB_TOKEN)" pipenv run python src/SKAOpenStack.py print_servers

get_machine_info: update_runners_from_machines ## Get Server Information from a GitLab Runner
	GITLAB_TOKEN="$(GITLAB_TOKEN)" pipenv run python src/SKAOpenStack.py print_server_info $(RUNNER_NAME)

update_runners_from_machines:
	ansible-playbook -i runnerhosts runner_info.yaml

list_all_runners: list_old_runners list_k8s_runners ## List all runners running on Engage Platform

list_k8s_runners: ## List Kubernetes Runners
	GITLAB_TOKEN="$(GITLAB_TOKEN)" pipenv run python src/Runner.py print_k8s_runners

list_old_runners: ## List non-Kubernetes Runners
	GITLAB_TOKEN="$(GITLAB_TOKEN)" pipenv run python src/Runner.py print_non_k8s_runners

check_runners: update_runners_from_machines ## Cross-check old runners from known engageska machines and gitlab API
	GITLAB_TOKEN="$(GITLAB_TOKEN)" pipenv run python src/Runner.py check_runners

delete_runner: ## Unregister a Runner with its id 
	GITLAB_TOKEN="$(GITLAB_TOKEN)" pipenv run python src/Runner.py unregister_runner --runner_id $(RUNNER_ID)

update_runner: ## Update a  Runner Configuration 
	GITLAB_TOKEN="$(GITLAB_TOKEN)" pipenv run python src/Runner.py update_runner --runner_id $(RUNNER_ID)

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
