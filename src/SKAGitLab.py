import configparser
from builtins import list

import gitlab
import os


class SKAGitLab(gitlab.Gitlab):
    group_id = 3180705

    def __init__(self, configfile="conf.ini"):

        config = configparser.ConfigParser()
        config.optionxform = str  # case sensitivity
        config.read(configfile)
        if os.environ.get("GITLAB_TOKEN"):
            gitlab_token = os.environ.get("GITLAB_TOKEN")
        else:
            gitlab_token = config["DEFAULT"]["gitlab-token"]
        gitlab.Gitlab.__init__(self, "https://gitlab.com", private_token=gitlab_token)

    def get_runners(self, tag_list=["engageska"]):
        ska_runners = []
        ska_projects = self.groups.get(self.group_id).projects.list()
        for proj in ska_projects:
            ska_proj = self.projects.get(proj.id, lazy=True)
            runners = ska_proj.runners.list(tag_list=tag_list)
            ska_runners.extend(runners)
        return list(set(ska_runners))  # eliminate duplicate items

    def get_runner(self, runner_id):
        return self.runners.get(runner_id)

    def get_k8s_runners(self):
        return self.get_runners(tag_list=["k8srunner"])

