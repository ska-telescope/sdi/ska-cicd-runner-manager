import openstack
import configparser
import fire
import csv
from texttable import Texttable


class ServerInfo:
    headers = [
        "Runner Machine Name",
        "id",
        "Flavor",
        "vCPUs",
        "RAM(MB)",
        "Disk (GB)",
        "Image",
    ]

    def __init__(self, name, id, flavor_name, vcpus, ram, disk, image):
        self.name = name
        self.id = id
        self.flavor_name = flavor_name
        self.vcpus = vcpus
        self.ram = ram
        self.disk = disk
        self.image = image

    def __str__(self):
        table = Texttable(max_width=120)
        table.set_deco(Texttable.HLINES | Texttable.HEADER)
        table.header(self.headers)
        table.add_rows(
            [
                [
                    self.name,
                    self.id,
                    self.flavor_name,
                    self.vcpus,
                    self.ram,
                    self.disk,
                    self.image,
                ]
            ]
        )
        return table.draw()


class SKAOpenStack:
    # Connect using env variables
    def connect(self):
        conn = openstack.connect()
        return conn

    def parse_config(self, configfile="conf.ini"):
        config = configparser.ConfigParser()
        config.optionxform = str  # case sensitivity
        config.read(configfile)
        servers = list(set(config["runners"].keys()) - set(config["DEFAULT"].keys()))
        return servers

    def get_runner_host(self, runner_name, runner_file="runner_names.txt"):
        runner_host = ""
        with open(runner_file) as rf:
            runner_reader = csv.DictReader(rf, fieldnames=["host", "executor", "name"])
            for runner in runner_reader:
                if runner_name == runner["name"].strip():
                    runner_host = runner["host"].strip()
        return runner_host

    def print_server_info(self, runner_name):
        runner_host = self.get_runner_host(runner_name)
        if not runner_host:
            print("Runner Host couldn't be found!")
        print(self.get_server(runner_host))

    def get_server(self, name_or_id):
        conn = self.connect()
        serv = conn.get_server(name_or_id=name_or_id)
        serv_volume = (
            conn.get_volume_by_id(serv["os-extended-volumes:volumes_attached"][0]["id"])
            if serv["os-extended-volumes:volumes_attached"]
            else None
        )
        return ServerInfo(
            serv["name"],
            serv["id"],
            serv["flavor"]["original_name"],
            serv["flavor"]["vcpus"],
            serv["flavor"]["ram"],
            serv_volume.size if serv_volume is not None else None,
            (
                serv_volume["volume_image_metadata"].image_name
                if serv_volume is not None
                else None
            ),
        )

    def get_servers(self):
        return [self.get_server(serv) for serv in self.parse_config()]

    def print_servers(self):
        table = Texttable(max_width=120)
        table.set_deco(Texttable.HLINES | Texttable.HEADER)
        servers = self.get_servers()
        servers_table = [
            [
                serv.name,
                serv.id,
                serv.flavor_name,
                serv.vcpus,
                serv.ram,
                serv.disk,
                serv.image,
            ]
            for serv in servers
        ]
        table.header(ServerInfo.headers)
        table.add_rows(servers_table, header=False)
        print(table.draw())


if __name__ == "__main__":
    # fire.Fire(SKAOpenStack)
    SKAOpenStack().print_servers()
