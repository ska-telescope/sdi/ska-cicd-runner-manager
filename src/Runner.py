import ast
import csv
import sys
import configparser
import fire
from texttable import Texttable

from SKAGitLab import SKAGitLab
from SKAOpenStack import ServerInfo


class Runner:
    headers = [
        "Name",
        "id",
        "Shared?",
        "Status",
        "Last Contacted at",
        "Tags",
        "Projects/Groups",
    ]
    configfile = "runnerconf.ini"

    def __init__(self, gitlab: SKAGitLab = SKAGitLab()):
        self.gitlab = gitlab
        self.gitlab_runners = self.gitlab.get_runners()
        self.k8s_runners = self.get_k8s_runners()
        self.runners_with_servers = dict.fromkeys(self.parse_openstack_runners(), None)

    def parse_openstack_runners(self, runner_file="runner_names.txt"):
        runner_names = []
        with open(runner_file) as rf:
            runner_reader = csv.DictReader(rf, fieldnames=["host", "executor", "name"])
            for runner in runner_reader:
                runner_names.append(runner["name"].strip())
        return runner_names

    def check_runners(self, runner_file="runner_names.txt"):
        missing_runners = []
        os_runners = self.parse_openstack_runners(runner_file)
        for runner in self.gitlab_runners:
            # no need to use set or bisect since this list is not that long
            if not runner.description in os_runners and not runner in self.k8s_runners:
                missing_runners.append(runner)
            else:
                self.runners_with_servers[runner.description] = runner
        if missing_runners:
            print("Note: The following runners are NOT hosted in Engage Platform!")
            self.print_runners(missing_runners)

    def unregister_runner(self, runner_id):
        runner = self.gitlab.get_runner(runner_id)
        runner.delete()
        print(f"The runner with id:{runner_id} is deleted (unregistered from GitLab))")

    def update_runner(self, runner_id):
        runner = self.gitlab.get_runner(runner_id)
        print("Current config:")
        self.print_runners([runner])

        config = configparser.ConfigParser()
        config.optionxform = str  # case sensitivity
        config["runner-config"] = {}
        settings = config["runner-config"]
        settings["active"] = str(runner.active)
        settings["tag_list"] = str(runner.tag_list)
        settings["run_untagged_jobs"] = str(runner.run_untagged)
        settings["lock_runner"] = str(runner.locked)
        with open(self.configfile, "w") as f:
            config.write(f)
        input(
            f"Settings are written in {self.configfile}, please update the values and press Enter..."
        )
        print("Updating Runner...")
        settings = self.parse_config()
        runner.active = True if settings["active"] == "True" else False
        runner.tag_list = ast.literal_eval(settings["tag_list"])
        runner.run_untagged = True if settings["run_untagged_jobs"] == "True" else False
        runner.locked = True if settings["lock_runner"] == "True" else False
        runner.save()
        print("Updated Runner:")
        self.print_runners([runner])

    def parse_config(self):
        config = configparser.ConfigParser()
        config.optionxform = str  # case sensitivity
        config.read(self.configfile)
        return config["runner-config"]

    def get_k8s_runners(self):
        runners = self.gitlab.get_k8s_runners()
        return runners

    def print_k8s_runners(self):
        print("Kubernetes Runners:")
        self.print_runners(self.k8s_runners)

    def print_non_k8s_runners(self):
        print("Non-Kubernetes Runners:")
        runners = [
            runner for runner in self.gitlab_runners if runner not in self.k8s_runners
        ]
        self.print_runners(runners)

    def print_runners(self, runners):
        table = Texttable(max_width=120)
        table.set_deco(Texttable.HLINES | Texttable.HEADER)
        formatted_runners = []
        for runner in runners:
            runner = self.gitlab.get_runner(runner.id)
            if runner.is_shared:
                projects = []
            else:
                if not runner.projects:
                    projects = [group["name"] for group in runner.groups]
                else:
                    projects = [proj["name"] for proj in runner.projects]
            formatted_runners.append(
                [
                    runner.description,
                    runner.id,
                    "True" if runner.is_shared else "False",
                    runner.status,
                    runner.contacted_at,
                    runner.tag_list,
                    projects,
                ]
            )
            # Sort by last contact time
            formatted_runners.sort(key=lambda x: x[4], reverse=True)
        table.header(Runner.headers)
        table.add_rows(formatted_runners, header=False)
        print(table.draw())


if __name__ == "__main__":
    fire.Fire(Runner)